//
//  ViewController.h
//  Recipe Scroll View
//
//  Created by Alex Brown on 5/15/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    IBOutlet UIScrollView *scroll;
    
}

@end

