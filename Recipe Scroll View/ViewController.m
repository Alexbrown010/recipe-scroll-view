//
//  ViewController.m
//  Recipe Scroll View
//
//  Created by Alex Brown on 5/15/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [scroll setScrollEnabled:YES];
    [scroll setContentSize:CGSizeMake(2500, 568)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
